## Buzzer-Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md)  

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab  

Locate the [buzzer speaker](https://www.amazon.com/Tegg-Passive-Speaker-Arduino-Raspberry/dp/B07PRTMF89) form your kit of stuff  

Note that the buzzer has 3 pins:  

VCC - for providing power to the module   
IO  - for providing the data signal to the module    
GND - for providing a common  return current path to complete the circuit for both VCC and IO nets  

In order to attach a buzzer we will need find an unused IO port in the schematic  

## Pick an IO port

Zoom into the U2 section and pick out a port. For this tutorial we'll use IO34  

Note that IO34 is connected to TP34; we an access this net at the TP34 testpint  

## Connect the wires

Connect TP34 net of the PCB to the IO pin of the buzzer module  

Connect 3.3V net of the PCB to the VCC pin of the buzzer module  

Connect GND net of the PCB to the GND net of the buzzer module  

When the IO signal is driven high, the buzzer membrane will move to its maximum  

When the IO signal is low, the buzzer membrane will move to its minimum  

The human ear can hear from about 50 Hertz or 50 Hz (meaning 50 cycles per second) until about 20 Kilo Hertz or 20KHz (meaning 20 x 1000 cycles per second)

The amazon website notes that this buzzer can produce tones up to 5 KHz  

## Code

Lucky for us the smart people at Arduino provide numerous examples that are easily accessible  

Navigate to File -> Examples -> 02.Digital -> toneMelody

Save the file as Arduino-Buzzer.ino

Note that at the top of the sketch it says "8 ohm speaker on digital pin 8"

We need to modify this sketch to drive IO34  

Locate and modify the lines of code that use port 8 modify it to port 34:

`tone(34, melody[thisNote], noteDuration);`

`noTone(34);`

Compile your code.  

Alas!  I doesn't compile!  

Turns out there is a [problem](https://github.com/espressif/arduino-esp32/issues/1720) that prevents the esp32 from getting tone and analogWrite working in a safe fashion.

Lucky for us some smart people on the internet have written a [custom library](https://github.com/lbernstone/Tone) to address this.

Close Arduino

Open a terminal and navigate to your libraries folder in your Sketchbook directory:
`cd ~/Arduino-Sketches/libraries`

Create a directory for the Tone32 library:
`mkdir Tone32`

Navigate to the new directory and use wget to download the [library files](https://github.com/lbernstone/Tone/tree/master/src)  
`cd Tone32`  
`wget https://github.com/lbernstone/Tone/blob/master/src/Tone32.cpp`  
`wget https://github.com/lbernstone/Tone/blob/master/src/Tone32.h`  
`wget https://github.com/lbernstone/Tone/blob/master/src/pitches.h`  

Open the Arduino IDE.

Navigate to Tools -> Manage Libraries

Filter for "Tone32" and install the library

The new noTone() function utilizes [ledcWrite()](https://github.com/espressif/arduino-esp32/blob/a59eafbc9dfa3ce818c110f996eebf68d755be24/cores/esp32/esp32-hal-ledc.h) requires an additional parameter to designate the channel value (0 in our case)

Modify the noTone() function call for the new parameter:

`noTone(34);`

Compile and upload your code.  Enjoy the Tunes!!

Here is a [fun site](https://noobnotes.net/popular/) with a bunch of melodies that list the individual note letters.

If you want to get fancy can navigate to File -> Examples -> 02.Digital -> toneMultiple and connect up multiple buzzers to the circuit so that you can play multiple tones simulaneously to make up a chord!

Happy Coding!



