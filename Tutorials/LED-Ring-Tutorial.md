## LED Ring Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md)  

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab  

Locate the [LED ring](https://www.amazon.com/DIYmall-WS2812-Light-Integrated-Drivers/dp/B0105VMUUQ) form your kit of stuff  

Note that the LED ring has 3 pins:  

PWR - voltage supply for the device

GND - return current path for all signals

IN  - input data signal

OUT - output data signal (use to daisy-chain LED rings together)

## Pick an IO port

Zoom into the U2 section and pick out a port. For this tutorial we'll use IO35  

Note that IO35 is connected to TP35; we an access this net at the TP35 testpint  

## Connect the wires

Connect TP35 net of the PCB to the IN pin of the LED ring   

Connect 3.3V net of the PCB to the PWR pin of the buzzer module  

Connect GND net of the PCB to the GND net of the buzzer module  

## Create new Sketchbook & Import Adafruit_NeoPixel library

Create a new Sketchbook by navigating to File -> New

Save the file as Arduino-LED-Ring.ino

The LED ring uses the [WS8212](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) LED controller.

Lucky for us some smart people a Adafruit have written the [Adafruit_NeoPixel](https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html) library to make it easier for us to code.

Navigate to Tools -> Manage Libraries

Filter for "Adafruit Neopixel" and install the library

## Code

At the top of your Sketchbook, add a line of code to include the Adafruit NeoPixel library:  
`#include <Adafruit_NeoPixel.h>`

Define a constant to reference our data port  
`#define data_pin 35`

Define the number of LED's on the Pixel Ring:  
`#define num_LEDs 12`

The NeoPixel object needs 3 parameters:  Number of LED's, Port Number, Pixel type  

We have 12 LED's, we are using IO port 35, and use the WS8212 which supports 800 KHz bitstream and uses the NEO_KHZ800 definition  

Instantiate the NeoPixel object:  
`Adafruit_NeoPixel ring = Adafruit_NeoPixel(numLEDs, data_pin, NEO_KHZ800);`  

Extend the setup function to start the strip, set the brightness and turn it on:
```
void setup() {
  ring.begin();                 // Initalize the ring
  ring.setBrightness(50);       // Set the brightness value
  ring.show();                  // Initialize all pixels to 'off'
}
```

Each LED has 3 independent colors

You can set the color of a pixel with [setPixelColor(n, red, green, blue)](https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html#ab8763ccc6f9a090df1f753905fd5561e) where:

n represent the LED number              (0 to 11)
red represents the Red intensity        (0 to 255)
green represents the Green intensity    (0 to 255)
blue represents the Blue intensity      (0 to 255)

Create and iniialize variables to assign to each LED number as well as each R, G, and B value:
```
int LED = 0;            //Which LED we are going to change
int R_Value = 0;        //The Red intensity
int G_Value = 0;        //The Green intensity
int B_Value = 0;        //The Blue intensity
```

Extend the loop function to use [random()](https://www.arduino.cc/reference/en/language/functions/random-numbers/random/) and [setPixelColor()](https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html#ab8763ccc6f9a090df1f753905fd5561e) and [random()](https://www.arduino.cc/reference/en/language/functions/random-numbers/random/) to infinitely light up random leds with arbitrary Red, Green, and Blue intensities: 
```
void loop() {
    LED = random(ring.numPixels());                     // Get a random LED
    R_Value = random(255);                              // Get a random Red intensity
    G_Value = random(255);                              // Get a random Green intensity
    B_Value = random(255);                              // Get a random Blue intensity 
    ring.setPixelColor(LED, R_Value, G_Value, B_Value); // Set the net parameters
    delay(20);                                          // Delay for 50 milleseconds, or change 5 LEDs each second
}
```
Compile and run your code!  Enjoy the Rainbow!

There are numerous tutorials and code snippets for the Adafruit_NeoPixel ring that do some elaborate displays of color and sequence.  

Use the world wide web to try out a few and find on you like!

