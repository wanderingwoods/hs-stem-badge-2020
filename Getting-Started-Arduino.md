# Getting started Arduino  

Before installing the Arduino environment be sure that you have prepared your Raspberry Pi as detailed in the [getting-started-raspberry-pi tutorial](Getting-Started-Raspberry-Pi.md)  

## Install Python Serial
Open a terminal window and use APT to install python serial library:  
`sudo apt install python-serial`  

## Install Arduino IDE  

We will use wget to download the Arduino integrated development environment (IDE) for Linux 32 bits.  Wget is a software package for retrieving files using HTTP, HTTPS, FTP, and FTPS which are the most widely used internet protocols.   Open a terminal and navigate to your downloads directory:  

`cd ~/Downloads`  

Use wget to download the latest version of Arduino (currently 1.18.12):  

`wget https://downloads.arduino.cc/arduino-1.8.12-linuxarm.tar.xz`  

Alternatively you can use a web browser to download Arduino by navigating to https://www.arduino.cc/en/Main/Software and clicking the Linux ARM 32bit version.   

Once we have the package we will need to unzip it using the tar command:  

`tar -xvf arduino-1.8.12-linuxarm.tar.xz`  

After tar has finished unpacking all the files we need to navigate to the newly created folder:  

`cd arduino-1.8.12`  

Then we need to run the install script with elevated privileges:  

`sudo ./install.sh`  

## Create Sketchbook location

Create a uniquely named directory for your Sketchbooks in your home directory:

'mkdir ~/Arduino-Sketchbooks'

## Run Arduino IDE

Once Arduino is finished installing arduino you should find it somewhere in the start menu.  Alternatively you can launch it from a terminal window with the command:  

`arduino` 

If you launch Arduino IDE from a terminal you can always kill it (or any program that is running in a terminal window) by selecting the terminal window and pressing Ctrl-c (press the control key and then while holding the control key press the c key).    

## Sketch directory and Additional Boards Manager URLs

In the Arduino IDE navigate to the preferences menu 

- File -> Preferences

In the "Sketchbook Location" field browse and pick the newly created Arduino-Sketchbooks folder. 

In the "Additional Boards Manager URLs" field add the location of the espressif json:  

`https://dl.espressif.com/dl/package_esp32_index.json`

**You can always add additional/multiple board manager urls by separating them with a comma  

Click OK to close the preferences dialog  

Close and restart the Arduino IDE.

## Select Board

In the Arduino IDE navigate to the Boards Manager menu:

 - Tools -> Board -> Boards Manager  

In the Boards Manager filter the available list by entering:

`esp32`
 
Look for esp32 by Espressif Systems (version 1.0.4 at the time of this writing) and install it.  

Once it is finished close and restart the Arduino IDE.

After restarting navigate to the Board menu and select the WROVER32 

 - Tools -> Board ->  ESP32 WRover Module  

### Set the Upload Speed

 - Tools -> Upload Speed -> "115200"
 
### Set the Flash Frequency

 - Tools ->Flash Frequency -> "80 MHz"
 
### Set the Flash Mode

 - Tools -> Flash Mode -> "DIO"
 
### Set the partition scheme

  - Tools -> Partition Scheme: -> "Minimal (2MB Flash)"

### Set the port

  - Tools -> Port -> "/dev/tty.USB0" 


  
  